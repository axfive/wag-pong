use core::ops::Neg;
use qwac::audio::{self, AudioNode, AudioSource, SingleValue};
use qwac::event::{ButtonState, Event, GamepadButton};
use qwac::texture::Texture;
use qwac::{DrawDestination, Screen};
use qwac::{Point, Rect, Rgba, Size};

fn abs<N>(number: N) -> N
where
    N: Neg<Output = N>,
    N: PartialOrd<N>,
    N: From<u8>,
{
    let zero = N::from(0u8);
    if number < zero {
        -number
    } else {
        number
    }
}

const MILLISECONDS_PER_PHYSICS_FRAME: u32 = 5;
const PADDLE_SIZE: f32 = 15.0;
const PADDLE_X_POSITION: f32 = 3.0;
const PADDLE_WIDTH: f32 = 2.0;
const BALL_SIZE: f32 = 5.0;
const SCREEN_SIZE: (f32, f32) = (160.0, 160.0);

#[derive(Debug)]
pub struct Game {
    screen: Screen,
    paddle_length: u8,
    paddle_position: f32,
    ball_position: (f32, f32),
    ball_direction: (f32, f32),
    holding_up: bool,
    holding_down: bool,
    physics_time: u32,
    score: u8,

    bounce_oscillator: audio::Oscillator,
    bounce_envelope: audio::Gain,
    echo_delay: audio::Delay,
    echo_gain: audio::Gain,

    fail_oscillator: audio::Oscillator,
    fail_envelope: audio::Gain,

    paddle_oscillator: audio::Oscillator,
    paddle_envelope: audio::Gain,

    number_texture: Texture,
}

enum Sound {
    Bounce,
    Paddle,
    Fail,
}

// The font for the numbers
const NUMBERS: [u8; 25] = [
    0b11101100, 0b11101110, 0b10101110, 0b11101110, 0b11101110, 0b10100100, 0b00100010, 0b10101000,
    0b10000010, 0b10101010, 0b10100100, 0b11101110, 0b11101110, 0b11100010, 0b11101110, 0b10100100,
    0b10000010, 0b00100010, 0b10100010, 0b10100010, 0b11101110, 0b11101110, 0b00101110, 0b11100010,
    0b11101110,
];

fn byte_to_colors(byte: u8) -> [Rgba; 8] {
    let mut colors = [Rgba::clear(); 8];

    for bit in 0..8usize {
        let set = ((byte >> (7 - bit)) & 1) == 1;
        if set {
            colors[bit] = Rgba::white();
        }
    }

    colors
}

impl Game {
    // Updated exactly once per physics frame, so we don't need to fool with delta here.
    fn physics_update(&mut self) {
        let ball_speed = self.score as f32 / 10.0 + 1.0;
        if self.holding_up {
            self.paddle_position -= ball_speed;
        }
        if self.holding_down {
            self.paddle_position += ball_speed;
        }

        if self.paddle_position < 0.0 {
            self.paddle_position = 0.0;
        } else if self.paddle_position > SCREEN_SIZE.1 - PADDLE_SIZE {
            self.paddle_position = SCREEN_SIZE.1 - PADDLE_SIZE;
        }

        self.ball_position.0 += ball_speed * self.ball_direction.0;
        self.ball_position.1 += ball_speed * self.ball_direction.1;

        let mut sound = None;

        if self.ball_position.0 > SCREEN_SIZE.0 - BALL_SIZE {
            sound = Some(Sound::Bounce);
            self.ball_direction.0 = -self.ball_direction.0;
            self.ball_position.0 = (SCREEN_SIZE.0 - BALL_SIZE) * 2.0 - self.ball_position.0;
        } else if self.ball_direction.0 < 0.0
            && self.ball_position.0 <= PADDLE_X_POSITION + PADDLE_WIDTH
            && ((self.paddle_position - BALL_SIZE)..=(self.paddle_position + PADDLE_SIZE))
                .contains(&self.ball_position.1)
        {
            sound = Some(Sound::Paddle);

            let percent = ((self.ball_position.1 - (self.paddle_position - BALL_SIZE))
                / (PADDLE_SIZE + BALL_SIZE))
                .clamp(0.2, 0.8);
            self.ball_direction.1 = (percent - 0.5) * 2.0;
            self.ball_direction.0 = 1.0 - abs(self.ball_direction.1);

            if self.ball_position.0 <= 0.0 {
                self.ball_position.0 = 1.0;
            }
            self.score = self.score.saturating_add(1);
        } else if self.ball_position.0 < 0.0 {
            sound = Some(Sound::Fail);
            self.ball_direction.0 = -self.ball_direction.0;
            self.ball_position.0 = -self.ball_position.0;
            self.score = 0;
        }

        if self.ball_position.1 > SCREEN_SIZE.1 - BALL_SIZE {
            sound = Some(Sound::Bounce);
            self.ball_direction.1 = -self.ball_direction.1;
            self.ball_position.1 = (SCREEN_SIZE.1 - BALL_SIZE) * 2.0 - self.ball_position.1;
        } else if self.ball_position.1 < 0.0 {
            sound = Some(Sound::Bounce);
            self.ball_direction.1 = -self.ball_direction.1;
            self.ball_position.1 = -self.ball_position.1;
        }

        match sound {
            Some(Sound::Bounce) => self.bounce_oscillator.play(),
            Some(Sound::Fail) => self.fail_oscillator.play(),
            Some(Sound::Paddle) => self.paddle_oscillator.play(),
            None => (),
        }
    }
}

impl qwac::Game for Game {
    fn new(_timestamp: f64) -> Self {
        let mut screen = Screen::get();

        screen.resize(SCREEN_SIZE.0 as _, SCREEN_SIZE.1 as _);

        let mut bounce_oscillator = audio::Oscillator::new(audio::OscillatorType::Sine);
        bounce_oscillator.set_value_at_time(0.0, 512.0);
        bounce_oscillator.exponential_ramp_to_value_at_time(-0.0, 1024.0);
        bounce_oscillator.length(0.25);

        let mut bounce_envelope = audio::Gain::new();
        bounce_oscillator.connect(&bounce_envelope);
        bounce_envelope.set_value_at_time(0.0, 0.0);
        bounce_envelope.linear_ramp_to_value_at_time(0.05, 0.8);
        bounce_envelope.exponential_ramp_to_value_at_time(0.1, 0.6);
        bounce_envelope.exponential_ramp_to_value_at_time(0.2, 0.5);
        bounce_envelope.linear_ramp_to_value_at_time(-0.0, 0.0);

        let mut echo_delay = audio::Delay::new(1.0);
        let mut echo_gain = audio::Gain::new();
        echo_delay.set_value(0.2);
        echo_gain.set_value(0.3);
        echo_delay.connect(&echo_gain);
        echo_gain.connect(&echo_delay);

        bounce_envelope.connect(&echo_delay);
        bounce_envelope.sink();
        echo_gain.sink();

        let mut fail_oscillator = audio::Oscillator::new(audio::OscillatorType::Triangle);
        fail_oscillator.set_value_at_time(0.0, 128.0);
        fail_oscillator.exponential_ramp_to_value_at_time(-0.0, 64.0);
        fail_oscillator.length(0.5);

        let mut fail_envelope = audio::Gain::new();
        fail_oscillator.connect(&fail_envelope);
        fail_envelope.set_value_at_time(0.0, 0.0);
        fail_envelope.linear_ramp_to_value_at_time(0.05, 0.8);
        fail_envelope.linear_ramp_to_value_at_time(-0.05, 0.8);
        fail_envelope.linear_ramp_to_value_at_time(-0.0, 0.0);

        fail_envelope.sink();

        let mut paddle_oscillator = audio::Oscillator::new(audio::OscillatorType::Sine);
        paddle_oscillator.set_value(1024.0);
        paddle_oscillator.length(0.5);

        let mut paddle_envelope = audio::Gain::new();
        paddle_oscillator.connect(&paddle_envelope);
        paddle_envelope.set_value_at_time(0.0, 0.0);
        paddle_envelope.linear_ramp_to_value_at_time(0.05, 1.0);
        paddle_envelope.linear_ramp_to_value_at_time(0.1, 0.3);
        paddle_envelope.linear_ramp_to_value_at_time(-0.1, 0.1);
        paddle_envelope.linear_ramp_to_value_at_time(-0.0, 0.0);

        paddle_envelope.sink();

        let number_texture = Texture::from_pixels(
            NUMBERS
                .iter()
                .copied()
                .flat_map(|byte| byte_to_colors(byte).into_iter()),
            5 * 8,
            5,
        );

        Game {
            paddle_length: 10,
            ball_position: (80.0, 80.0),
            paddle_position: 0.0,
            ball_direction: (0.8, 0.2),
            holding_up: false,
            holding_down: false,
            physics_time: 0,
            score: 0,
            bounce_oscillator,
            bounce_envelope,
            echo_delay,
            echo_gain,
            fail_envelope,
            fail_oscillator,
            paddle_envelope,
            paddle_oscillator,
            number_texture,
            screen,
        }
    }

    fn event(&mut self, event: Event) {
        match event {
            Event::GamepadButton {
                button: GamepadButton::Up,
                state: ButtonState::Pressed,
                ..
            } => {
                self.holding_up = true;
            }
            Event::GamepadButton {
                button: GamepadButton::Up,
                state: ButtonState::Released,
                ..
            } => {
                self.holding_up = false;
            }
            Event::GamepadButton {
                button: GamepadButton::Down,
                state: ButtonState::Pressed,
                ..
            } => {
                self.holding_down = true;
            }
            Event::GamepadButton {
                button: GamepadButton::Down,
                state: ButtonState::Released,
                ..
            } => {
                self.holding_down = false;
            }
            _ => (),
        }
    }

    fn update(&mut self, frame_milliseconds: u32) {
        // Allow for a constant physics timestep.
        self.physics_time += frame_milliseconds;
        while let Some(physics_time) = self
            .physics_time
            .checked_sub(MILLISECONDS_PER_PHYSICS_FRAME)
        {
            self.physics_update();
            self.physics_time = physics_time;
        }

        // Render the field and ball at least
        self.screen.draw_rectangle(
            Rect {
                origin: Point::new(0.0, 0.0),
                size: Size::new(SCREEN_SIZE.0, SCREEN_SIZE.1),
            },
            Rgba {
                r: u8::MIN,
                g: u8::MIN,
                b: u8::MIN,
                a: u8::MAX,
            },
        );

        let ball_rectangle = Rect {
            origin: Point::new(self.ball_position.0, self.ball_position.1),
            size: Size::new(BALL_SIZE, BALL_SIZE),
        };

        let paddle_rectangle = qwac::Rect {
            origin: Point::new(PADDLE_X_POSITION, self.paddle_position),
            size: Size::new(PADDLE_WIDTH, PADDLE_SIZE),
        };

        self.screen.draw_rectangle(
            ball_rectangle,
            Rgba {
                r: u8::MAX,
                g: u8::MAX,
                b: u8::MAX,
                a: u8::MAX,
            },
        );

        self.screen.draw_rectangle(
            paddle_rectangle,
            Rgba {
                r: u8::MAX,
                g: u8::MAX,
                b: u8::MAX,
                a: u8::MAX,
            },
        );

        let score_parts = [self.score / 100, self.score % 100 / 10, self.score % 10];

        for (i, digit) in score_parts.into_iter().enumerate() {
            self.screen.draw_texture(
                &self.number_texture,
                Rect {
                    origin: Point::new((digit * 4) as f32, 0.0),
                    size: Size::new(4.0, 5.0),
                },
                Rect {
                    origin: Point::new(SCREEN_SIZE.0 - 15.0 + (i * 4) as f32, 5.0),
                    size: Size::new(4.0, 5.0),
                },
            );
        }
    }
}
